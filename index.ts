import { lstatSync, readdirSync } from "fs";
import path from "path";
import * as config from "./config.json";
import MusicBot from "./music-bot";

const prefix: string = config.prefix;
const token: string = config.token;
const startVolume: number = config.startVolume / 100;

const urlTracks = config.tracks.filter((val) => !!val.url) as {
  title: string;
  url: string;
  volumeMultiplier?: number;
}[];

const fileTracks = config.tracks.filter((val) => !!val.file) as {
  title: string;
  file: string;
  volumeMultiplier?: number;
}[];

// List of files in the music/ folder, plus any file sounds in the config.json.
// Duplicate file paths use the one in the config.
const files = [
  ...readdirSync("music")
    .map((filename) => ({
      file: path.resolve("music", filename),
      title: path.parse(filename).name,
      volumeMultiplier: undefined,
    }))
    .filter(
      (filename) =>
        lstatSync(filename.file).isFile() &&
        !fileTracks.some(
          (configTrack) =>
            filename.file === path.resolve("music", configTrack.file)
        )
    ),
  ...fileTracks,
];

const bot = new MusicBot(prefix, token, startVolume, files, urlTracks);

bot.client.on("ready", () => console.log("Ready!"));

/*
 * Gracefully shutdown
 */
process.on("SIGINT", () => exit_handler());
process.on("SIGTERM", () => exit_handler());

let exited = false;
function exit_handler() {
  if (!exited) {
    console.log("Exiting");
    bot.destroy();
  }

  exited = true;
}
